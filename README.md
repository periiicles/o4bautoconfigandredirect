This script automatically configures the Onedrive for Business client for your users, and sets up folder redirection.
For more info see http://www.lieben.nu/liebensraum/o4bclientautoconfig/